#!/bin/bash

# Check parameter
if (( $# != 1)); then
	echo "Usage: write_bootloader.sh /dev/sdX where sdX is the disk name."
	exit 1
fi

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Writing bootloader image to ${1}..."

sudo dd if=u-boot.imx of=${1} bs=1K seek=1; sync

echo "Bootloader image written..."

