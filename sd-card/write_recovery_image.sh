#!/bin/bash

# Check parameter
if (( $# != 1)); then
	echo "Usage: write_recovery_image.sh /dev/sdX where sdX is the disk name."
	exit 1
fi

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Writing recovery image to ${1}2..."

dd if=recovery-hummingboard.img of=${1}2; sync

echo "Image written..."

