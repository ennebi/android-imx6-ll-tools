#!/bin/bash

# Check parameter
if (( $# != 1)); then
	echo "Usage: write_system_image.sh /dev/sdX where sdX is the disk name."
	exit 1
fi

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Writing system image to ${1}5..."

dd if=system.img of=${1}5; sync

echo "Image written..."

