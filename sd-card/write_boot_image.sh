#!/bin/bash

# Check parameter
if (( $# != 1)); then
	echo "Usage: write_boot_image.sh /dev/sdX where sdX is the disk name."
	exit 1
fi

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Writing boot image to ${1}1..."

dd if=boot-hummingboard.img of=${1}1; sync

echo "Image written..."

