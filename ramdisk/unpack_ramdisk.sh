#!/bin/bash

if (( $# != 1)); then
	echo "Usage: unpack_ramdisk.sh ramdisk_file"
	exit 1
fi

cp $1 input_ramdisk.cpio.gz
gunzip input_ramdisk.cpio.gz
mkdir ramdisk;
cd ramdisk
cpio -i < ../input_ramdisk.cpio

