#!/bin/bash

if (( $# != 1)); then
	echo "Usage: unpack_ramdisk.sh ramdisk_folder"
	exit 1
fi

cd $1
find . | cpio --create --format='newc' | gzip > ../output-ramdisk.img

