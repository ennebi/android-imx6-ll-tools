# Android image TOOLS #

## pack_bootimage.sh ##
A script that packs zImage, platform DTB files, command line and ramdisk image in one package.
A similar script is provided to pack recovery image.

Usage:
Make sure that, in your current folder, there are the following files:

* zImage: the compiled kernel image
* ramdisk.img: the ramdisk image
* ramdisk-recovery.img: the recovery ramdisk image
* imx6dl-hummingboard.dtb: the device tree binary file

```
./pack_bootimage.sh
./pack_recovery.sh
```

You may run the script also from another directory (e.g. you may have a folder containing all the required files and put these scripts in your PATH).

The boot image is packed as boot-hummmingboard.img. Recovery image is recovery-hummingboard.sh.
The default command line can be overridden from u-boot command line.

## SD card shortcut scripts ##

The four scripts (write_bootloader.sh, write_boot_image.sh, write_recovery_image.sh and write_system_image.sh) are used to write to the correct partition the three images needed by the platform.

Usage is the same for all the scripts (it just changes the destination partition and the source file):

```
sudo ./write_boot_image.sh /dev/sdZ # sdZ represent the mount point of your SD card disk.
```

Make sure that in your current path the following files are existing:

* u-boot.imx: for write_bootloader.sh compiled uboot (note the imx extension, it is a special output format from u-boot)
* boot-hummingboard.img for write_boot_image.sh
* recovery-hummingboard.img for write_recovery_image.sh
* system.img for write_system_image.sh

## Ramdisk tools ##

Two tools are provided to unpack and repack ramdisk: unpack_ramdisk.sh and repack_ramdisk.sh

Usage:

```
./unpack_ramdisk.sh ramdisk.img
```

This command creates a folder named ramdisk in the working directory. Note that this directory will contain init scripts, fstab file, and minimal binaries needed to boot the device.

```
./repack_ramdisk ramdisk
```

This command will repack the ramdisk directory by creating a file named output-ramdisk.img

# How to build Android distribution #

Follow this guide to build your Android distribution or download prebuilt packages:

https://bitbucket.org/ennebi/android-imx6-ll-tools/wiki/
