#!/bin/sh

echo "Generating boot-hummingboard.img..."

# NOTE: Freescale recommends CMA=384M. On systems with less than 1GB ram, about three times the framebuffer size should be used.

`dirname $0`/mkbootimg  --kernel zImage --ramdisk ramdisk.img --output boot-hummingboard.img --cmdline "console=ttymxc0,115200 init=init video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24,bpp=32 video=mxcfb1:off video=mxcfb2:off video=mxcfb3:off vmalloc=400M androidboot.console=ttymxc0 consoleblank=0 androidboot.hardware=freescale cma=220M" --base 0x14000000 --second imx6dl-hummingboard.dtb

echo "boot-hummmingboard.img generated."
